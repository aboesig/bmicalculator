//
//  ViewController.swift
//  BMIcalculator
//
//  Created by Andreas Boesig on 15/11/2016.
//  Copyright © 2016 Andreas Boesig. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var bmiResult: UILabel!
    @IBOutlet weak var bmiWeight: UILabel!
    @IBOutlet weak var bmiHeight: UILabel!
    
    var h:Float = 1.5
    var w:Float = 75
    var bmi:Float = 0;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func weightChg(_ sender: UISlider) {
        
        w = sender.value
        bmiWeight.text = NSString(format: " %.2f kg", w) as String
        
    }
    
    @IBAction func heightChg(_ sender: UISlider) {
        
        h = sender.value
        bmiHeight.text = NSString(format: " %.2f m", h) as String
        
    }
    
    @IBAction func calcBMI(_ sender: Any) {
        
        bmi = w / (h*h)
        
        var str = ""
        
        if bmi < 18.5
        {
            str = NSString(format: "Your BMI is %.2f, Under weight", bmi) as String
            bmiResult.textColor = UIColor.red
            
        } else if bmi >= 18.5 && bmi < 25
        {
            
            str = NSString(format: "Your BMI is %.2f, Normal weight", self.bmi) as String
            self.bmiResult.textColor = UIColor.green
            
        } else if bmi > 25 && bmi < 30
        {
            
            str = NSString(format: "Your BMI is %.2f, Overweight", bmi) as String
            bmiResult.textColor = UIColor.yellow
            
        } else
        {
            
            str = NSString(format: "Your BMI is %.2f, Obese", bmi) as String
            bmiResult.textColor = UIColor.red
            
        }
        
        bmiResult.text = str
        
    }

}

